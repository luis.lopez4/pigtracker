import ExampleComponent from './components/ExampleComponent.vue';
import ModalComponent from './components/ModalComponent.vue'
import './bootstrap';

window.Vue = require('vue');
Vue.component('example-component', ExampleComponent);
Vue.component('modal-component',ModalComponent);
const app = new Vue({
    el: '#app',
    data: {
        tituloGasto: 'Agregar gasto',
        gastos: [
            {concepto:'Spotify',cantidad:149.5},
            {concepto: 'Renta',cantidad: 0},
            {concepto: 'Super',cantidad: 1200},
            {concepto:'transferencia',cantidad:300},
        ],
        nuevoGastoPrueba:'',
        nuevaCantidadPrueba:'',
        nuevoGasto: 'nuevoGastoPrueba',
        nuevaCantidad: 'nuevaCantidadPrueba'
    },
    methods: {
        agregarGasto(){
            this.gastos.push({
                concepto: this.nuevoGasto, cantidad: this.nuevaCantidad
            })
            this.nuevoGasto= '',
            this.nuevaCantidad=''
        },

        borrarGasto(index) {
           this.gastos.splice(index, 1);
        }
    },
    computed:{
        SumarGastos(){
            this.total = 0;
            // for(gasto of this.gastos){
            //     this.total = this.total + gasto.cantidad;
            // }
              return this.total;
        }
    },mounted() {
        console.log("estoy aqui");
    },
});
