<!DOCTYPE html>
<html lang="en">
<div class="head">
    @yield('head')

    <head>
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Inicio</title>
    </head>
</div>

<body>
    <div class="principal">
        <aside class="lateral">
            @yield('lateral')
            <img class="logoImg" src="{{ asset('images/Pig.png') }}" alt="">
            <h1><a href="#">General</a></h1>
            <h1><a href="#">Proximos pagos</a></h1>
            <h1><a href="#">Estadisticas</a></h1>

            <h1><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Cerrar sesion
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </h1>
        </aside>
        <header class="cabeza">
            @yield('cabeza')

        </header>
        <div id="app" class="cuerpo">
            @yield('cuerpo')
           
        </div>
        <footer class="pie">
            @yield('pie')
            <div class="menuCelular">
                <h1><a href="#">General</a></h1>
                <h1><a href="#">Proximos pagos</a></h1>
                <h1><a href="#">Estadisticas</a></h1>

                <h1><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Cerrar sesion
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </h1>

            </div>
            <hr>
            <h3>Copyright © Todos los derechos reservados</h3>

            <script src="{{ asset('js/app.js') }}" defer></script>
            <script src="{{ asset('js/main.js') }}" defer></script>
        </footer>
    </div>


</body>

</html>