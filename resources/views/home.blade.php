@extends('layouts.plantilla')
<!DOCTYPE html>
@section('head')
@endsection
<link href="{{ asset('css/main.css') }}" rel="stylesheet">

<body>
    @section('lateral')
    @endsection
    @section('cabeza')
    @endsection
    @section('cuerpo')
    <div class="finanzas">
        <div class="gastos">
            <div class="botonContenedor">
                <button id="abrirModal" class="boton">@{{tituloGasto}}</button>
            </div>
            <example-component></example-component>
           <modal-component></modal-component>
            <div class="gastosPrincipal">
                <table style="width:100%; border-collapse:collapse; ">
                    <tr>
                        <td colspan="2" style="border: 1px solid black; background-color:#DAAFBE; text-align:center;">Concepto</td>
                        <td style="border: 1px solid black; background-color:#DAAFBE;text-align:center;">Cantidad (MXN)</td>
                    </tr>
                    <tr v-for="(gasto, index) of gastos">
                        <td colspan="2" style="border: 1px solid black;">@{{gasto.concepto}}</td>
                        <td style="border: 1px solid black;" align="right" :class="gasto.cantidad==0? 'gastoCero' : ''  ">$@{{gasto.cantidad}}</td>
                        <td style="border: none"><button @click="borrarGasto(index)">X</button></td>
                    </tr>
                </table>

                <div class="gastosTotales">
                    <h1>Gastos totales: @{{SumarGastos}} MXN</h1>
                    <p>*Los gastos en cero no son sumados,se recomienda eliminarlos.</p>
                </div>

            </div>

        </div>
        <div class="ganancias">

        </div>
        <div class="categorias">

        </div>
    </div>












    @endsection
    @section('pie')
    @endsection
</body>

</html>