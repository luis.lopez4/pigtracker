 var modal = document.getElementById('spanModal');
 var modalBtn = document.getElementById('abrirModal');
 var modalClose = document.getElementById('cerrarModal');
 var aceptarClose = document.getElementById('agregarGasto');

 modalBtn.addEventListener('click', openModal);
 modalClose.addEventListener('click', cerrarModal);
 aceptarClose.addEventListener('click', cerrarModal);

 function openModal() {
     modal.style.display = "block";
 }

 function cerrarModal() {
     modal.style.display = "none";
 }